provider "google" {
  alias = "tokengen"
}

data "google_service_account_access_token" "sa" {
  provider               = google.tokengen
  target_service_account = "iac-project@prismatic-petal-395711.iam.gserviceaccount.com"
  scopes                 = ["userinfo-email", "cloud-platform"]
  lifetime               = "3600s"
}

provider "google" {
  access_token = data.google_service_account_access_token.sa.access_token
  project      = "prismatic-petal-395711"
  region       = "asia-east1"
}

data "google_client_openid_userinfo" "me" {
  provider = google
}

output "target-email" {
  value = data.google_client_openid_userinfo.me.email
}

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.82.0"
    }
  }
  required_version = "~> 1.5"
  backend "gcs" {
    bucket = "test-gke-tf-staging"
    prefix = "atlantis"
  }
}

resource "google_compute_instance" "test" {
  name         = "test-vm"
  machine_type = "e2-micro"
  zone         = "asia-east1-b"
  network_interface {
    network = "default"
  }
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
      labels = {
        my_label = "value"
      }
      type = "pd-standard"
      size = "10"
    }
  }
}

output "vm_internal_ip" {
  value = google_compute_instance.test.network_interface.0.network_ip
}
